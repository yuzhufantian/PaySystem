package com.so206.service.impl;

import com.so206.mapper.SystemPayConfigMapper;
import com.so206.po.SystemPayConfigWithBLOBs;
import com.so206.service.PayConfigService;
import com.so206.utils.BeanCheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = RuntimeException.class)
public class PayConfigServiceImpl implements PayConfigService {

    @Autowired
    private SystemPayConfigMapper payConfigMapper;

    @Override
    public SystemPayConfigWithBLOBs getPayConfig(Integer id) {
        return payConfigMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updatePPayConfig(SystemPayConfigWithBLOBs payConfig) {
        SystemPayConfigWithBLOBs config = payConfigMapper.selectByPrimaryKey(payConfig.getId());
        if (config != null) {
            BeanCheck.copyPropertiesIgnoreNull(payConfig, config);
            payConfigMapper.updateByPrimaryKeyWithBLOBs(config);
        }
    }

}
